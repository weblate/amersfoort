export interface Room {
  /**
   * The code of the room.
   */
  roomCode: string;

  /**
   * A human readable name of the room.
   */
  roomName: string;

  /**
   * The total number of reservations allowed in the room.
   */
  capacity: number;
}

export interface Location {
  /**
   * The code of the location.
   */
  locationCode: string;

  /**
   * A human readable name of the location.
   */
  locationName: string;

  /**
   * The list of rooms associated with the location.
   */
  rooms: Room[];
}

export interface Building {
  /**
   * The code of the location’s building.
   */
  buildingCode: string;

  /**
   * The name of the location’s building.
   */
  buildingName: string;

  /**
   * The list of locations associated with the building.
   */
  locations: Location[];
}

/**
 * A partial reservation for the form.
 */
export interface Reservation {
  /**
   * The ID of the resource that’s being modified.
   */
  id?: number;

  /**
   * The date of a reservation.
   */
  date: string;

  /**
   * The building of a reservation.
   */
  building: string;

  /**
   * The location of a reservation.
   */
  location: string;

  /**
   * The room of the reservation.
   */
  room: string;

  /**
   * The time of the reservation.
   */
  time: 1 | 2 | 3;
}

export interface Team {
  /**
   * The ID of the team
   */
  id: number;

  /**
   * The name of the team.
   */
  name: string;

  /**
   * The role of the user within the team.
   */
  role: 'manager' | 'member';

  /**
   * The total amount of members of the team.
   */
  size: number;

  /**
   * The custom annotations in the team.
   */
  annotations: Record<string, string>;
}
