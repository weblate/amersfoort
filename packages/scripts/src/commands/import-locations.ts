import { readFile } from 'fs/promises';
import { URL } from 'url';

import { logger } from '@appsemble/node-utils';
import axios from 'axios';
import * as parseCSV from 'csv-parse/lib/sync';
import { Argv } from 'yargs';

export const command = 'import-locations';
export const description = 'Import locations for the werkplekken app from a CSV file';

interface Args {
  /**
   * The path to the CSV file that contains the locations to import
   */
  inputPath: string;

  /**
   * The remote to upload to.
   */
  remote: string;

  /**
   * The ID of the app on the remote to upload to.
   */
  id: number;
}

export interface Room {
  /**
   * The code of the room.
   */
  roomCode: string;

  /**
   * A human readable name of the room.
   */
  roomName: string;

  /**
   * The total number of reservations allowed in the room.
   */
  capacity: number;
}

export interface Location {
  /**
   * The code of the location.
   */
  locationCode: string;

  /**
   * A human readable name of the location.
   */
  locationName: string;

  /**
   * The list of rooms associated with the location.
   */
  rooms: Room[];
}

export interface Building {
  /**
   * The code of the location’s building.
   */
  buildingCode: string;

  /**
   * The name of the location’s building.
   */
  buildingName: string;

  /**
   * The list of locations associated with the building.
   */
  locations: Location[];

  /**
   * The priority for sorting the building
   */
  priority: number;
}

interface CSVRecord {
  /**
   * The code of the building
   */
  buildingCode: string;

  /**
   * The label of the building
   */
  buildingName: string;

  /**
   * The priority of the building
   */
  buildingPriority: string;

  /**
   * The code of the location
   */
  locationCode: string;

  /**
   * The label of the location
   */
  locationName: string;

  /**
   * The code of the room
   */
  roomCode: string;

  /**
   * The label of the room
   */
  roomName: string;

  /**
   * The total capacity of the room
   */
  roomCapacity: string;
}

/**
 * Parses the import-locations command.
 *
 * @param argv - The arguments for the command.
 * @returns The arguments that were passed through
 */
export function build(argv: Argv): Argv {
  return argv
    .positional('inputPath', {
      desc: 'The remote to download data from',
    })
    .option('remote', {
      desc: 'The remote to upload data into',
      default: 'http://localhost:9999',
    })
    .option('id', {
      type: 'number',
      desc: 'The app id to upload data into',
      demandOption: true,
    });
}

/**
 * Runs the import-locations command.
 *
 * @param argv - The arguments for the command.
 */
export async function handler({
  id,
  inputPath,
  remote = 'https://appsemble.app',
}: Args): Promise<void> {
  /**
   * The host to upload data to, i.e. http://localhost:9999.
   */
  const uploader = axios.create({
    baseURL: String(new URL(`/api/apps/${id}/resources`, remote)),
  });

  const data = await readFile(inputPath, 'utf-8');
  const csv: CSVRecord[] = parseCSV(data, { columns: true, delimiter: ';' });
  const buildings = new Map<string, Building>();

  csv.forEach((record) => {
    let building = buildings.get(record.buildingCode)!;
    if (!building) {
      building = {
        buildingCode: record.buildingCode,
        buildingName: record.buildingName,
        priority: Number.parseInt(record.buildingPriority),
        locations: [],
      };
      buildings.set(record.buildingCode, building);
    }
    let location = building.locations.find((loc) => loc.locationCode === record.locationCode);
    if (!location) {
      location = {
        locationCode: record.locationCode,
        locationName: record.locationName,
        rooms: [],
      };
      building.locations.push(location);
    }
    location.rooms.push({
      roomCode: record.roomCode,
      roomName: record.roomName,
      capacity: Number.parseInt(record.roomCapacity),
    });
  });

  for (const building of buildings.values()) {
    logger.info(`Creating building ${building.buildingCode}`);
    await uploader.post('location', building);
  }
}
